#!/bin/bash
vlc=/mnt/c/Program\ Files\ \(x86\)/VideoLAN/VLC/vlc.exe
taskkill=/mnt/c/Windows/system32/taskkill.exe

if ! command -v "$vlc" &> /dev/null; then
	echo "VLC not found at $vlc. This script requires vlc."
	exit
fi

if ! command -v "$taskkill" &> /dev/null; then
	echo "taskkill not found at $taskkill. This script requires taskkill. Are you running the script on WSL?"
	exit
fi

if ! command -v mediainfo &> /dev/null; then
	echo "This script requires mediainfo. Exiting"
	exit
fi

function cleanExit() {
	echo "Caught exit signal, killing vlc and exiting"
	"$taskkill" /F /IM vlc.exe
	exit $?
}

trap cleanExit SIGINT
trap cleanExit SIGTERM

# Assumes that the working directory contains the files to sort

for musicFile in *.{mp3,flac,wav}; do
	clear
        printf "Sorting {%s}..." "$musicFile"
	if [ ! -f "$musicFile" ]; then
		echo "file $musicFile does not exist, skipping this one..."
		continue
	fi

	# Start listening at a third of the total duration
	start=$(($(mediainfo --Inform="Audio;%Duration%" "$musicFile") / 3000)) 
	
	"$vlc" --start-time=$start "$musicFile" --intf dummy &
	echo -e "\nEnter destination directory: "
	read destFolder
	until [ -d $destFolder ]; do
		echo "$destFolder is not a directory."
		echo -ne "\nEnter destination directory: "
		read destFolder
	done
	"$taskkill" /F /IM vlc.exe &> /dev/null
	echo "moving track to $destFolder"
	mv $musicFile $destFolder
done
